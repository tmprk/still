//
//  TextfieldMenuItem.swift
//  Still
//
//  Created by Timothy Park on 6/28/19.
//  Copyright © 2019 Timothy Park. All rights reserved.
//

import Cocoa

protocol AddTaskProtocol: class {
    func addTask(title: String, completed: Bool)
}

final class TextfieldMenuItem: NSView, NibLoadable {
    
    weak var delegate: AddTaskProtocol?
    @IBOutlet weak var textfield: Textfield!
    var onClick: ((NSClickGestureRecognizer) -> ())?
    
    private var gestureRecognizer: NSClickGestureRecognizer!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        gestureRecognizer = NSClickGestureRecognizer(target: self, action: #selector(clicked(_:)))
        addGestureRecognizer(gestureRecognizer)
    }
    
    override func acceptsFirstMouse(for event: NSEvent?) -> Bool {
        return true
    }
    
    @objc private func clicked(_ sender: NSClickGestureRecognizer) {
        onClick?(sender)
    }
    
    override func viewDidMoveToWindow() {
        super.viewDidMoveToWindow()
        self.textfield.window?.makeFirstResponder(self.textfield)
    }
    
    @IBAction func pressedEnter(_ sender: Any) {
        if !(textfield.stringValue == "") {
            self.delegate?.addTask(title: textfield.stringValue, completed: true)
            self.textfield.stringValue = ""
        } else {
            print("textfield is empty")
            return
        }
    }
    
}
