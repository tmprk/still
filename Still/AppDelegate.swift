//
//  AppDelegate.swift
//  Still
//
//  Created by Timothy Park on 6/27/19.
//  Copyright © 2019 Timothy Park. All rights reserved.
//

import Cocoa
import CoreData

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate, NSMenuDelegate, AddTaskProtocol {
    
    @IBOutlet weak var statusMenu: NSMenu!
    let statusItem = NSStatusBar.system.statusItem(withLength: NSStatusItem.variableLength)
    private var textfieldMenuItemView: TextfieldMenuItem?
    var tasks: [NSManagedObject] = []

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        fetchTasks()
        generalSetup()
        setupStatusItem()
        setupStatusItemBehavior()
        print("items in array: \(tasks.count)")
    }
    
    func generalSetup() {
        statusMenu.delegate = self
    }
    
    func setupStatusItem() {
        if let button = statusItem.button {
            button.title = "Replace Me"
        }
    }
    
    func itemClicked() {
        NSApp.activate(ignoringOtherApps: true)
        self.statusItem.menu = self.statusMenu
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.statusItem.button?.performClick(self)
        }
    }
    
    @objc func doSomething(sender: NSMenuItem) {
        print(sender.tag)
        self.statusItem.button?.title = sender.title
    }
    
    private func setupStatusItemBehavior() {
        NSEvent.addLocalMonitorForEvents(matching: NSEvent.EventTypeMask(type: .leftMouseDown), handler: { event in
            if event.window == self.statusItem.button?.window {
                self.itemClicked()
                return nil
            }
            return event
        })
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Still")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error {
                fatalError("Unresolved error \(error)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving and Undo support
    
    @IBAction func saveAction(_ sender: AnyObject?) {
        let context = persistentContainer.viewContext
        if !context.commitEditing() {
            NSLog("\(NSStringFromClass(type(of: self))) unable to commit editing before saving")
        }
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Customize this code block to include application-specific recovery steps.
                let nserror = error as NSError
                NSApplication.shared.presentError(nserror)
            }
        }
    }
    
    func windowWillReturnUndoManager(window: NSWindow) -> UndoManager? {
        return persistentContainer.viewContext.undoManager
    }
    
    func applicationShouldTerminate(_ sender: NSApplication) -> NSApplication.TerminateReply {
        // Save changes in the application's managed object context before the application terminates.
        let context = persistentContainer.viewContext
        
        if !context.commitEditing() {
            NSLog("\(NSStringFromClass(type(of: self))) unable to commit editing to terminate")
            return .terminateCancel
        }
        
        if !context.hasChanges {
            return .terminateNow
        }
        
        do {
            try context.save()
        } catch {
            let nserror = error as NSError
            
            // Customize this code block to include application-specific recovery steps.
            let result = sender.presentError(nserror)
            if (result) {
                return .terminateCancel
            }
            
            let question = NSLocalizedString("Could not save changes while quitting. Quit anyway?", comment: "Quit without saves error question message")
            let info = NSLocalizedString("Quitting now will lose any changes you have made since the last successful save", comment: "Quit without saves error question info");
            let quitButton = NSLocalizedString("Quit anyway", comment: "Quit anyway button title")
            let cancelButton = NSLocalizedString("Cancel", comment: "Cancel button title")
            let alert = NSAlert()
            alert.messageText = question
            alert.informativeText = info
            alert.addButton(withTitle: quitButton)
            alert.addButton(withTitle: cancelButton)
            
            let answer = alert.runModal()
            if answer == .alertSecondButtonReturn {
                return .terminateCancel
            }
        }
        // If we got here, it is time to quit.
        return .terminateNow
    }
    
}

extension AppDelegate {
    
    func addTask(title: String, completed: Bool) {
        save(title: title, completed: false)
        reloadMenu()
//        let indexToAdd = statusMenu.numberOfItems - 4
//        let taskItem = NSMenuItem(title: title, action: #selector(doSomething), keyEquivalent: "")
//        taskItem.tag = statusMenu.numberOfItems - 6
//        statusMenu.insertItem(taskItem, at: indexToAdd)
    }
    
    @objc private func reloadMenu() {
        statusMenu.removeAllItems()
        
        // textfield item
        let textfieldItem = NSMenuItem()
        textfieldMenuItemView = makeTextfieldMenuItem()
        textfieldMenuItemView?.delegate = self
        textfieldItem.view = textfieldMenuItemView
        statusMenu.addItem(textfieldItem)
        
        // seperator item
        let seperator = makeSeperatorItem()
        statusMenu.addItem(seperator)
        
        let taskItems = makeTaskItems()
        taskItems.forEach { statusMenu.addItem($0) }
        
        // seperator item
        let secondSeperator = makeSeperatorItem()
        statusMenu.addItem(secondSeperator)
        
        // clear item
        let clearItem = makeClearItem()
        statusMenu.addItem(clearItem)
        
        // seperator item
        let thirdSeperator = makeSeperatorItem()
        statusMenu.addItem(thirdSeperator)
        
        // quit item
        let quitItem = makeQuitItem()
        statusMenu.addItem(quitItem)
    }
    
    @objc private func makeTaskItems() -> [NSMenuItem] {
        return tasks.enumerated().map {
            let title = $0.element.value(forKey: "title") as! String
            let item = NSMenuItem(title: title, action: #selector(doSomething), keyEquivalent: "")
            item.tag = $0.offset
            item.target = self
            return item
        }
    }
    
    private func makeTextfieldMenuItem() -> TextfieldMenuItem? {
        guard let textfieldMenuItemView = TextfieldMenuItem.createFromNib() else {
            return nil
        }
        return textfieldMenuItemView
    }
    
    private func makeCompletableMenuItem(taskName: String) -> CompletableItem? {
        guard let completableMenuItemView = CompletableItem.createFromNib() else {
            return nil
        }
        completableMenuItemView.configure(taskName: taskName)
        return completableMenuItemView
    }
    
    private func makeClearItem() -> NSMenuItem {
        let item = NSMenuItem(title: "Clear Items", action: #selector(deleteData), keyEquivalent: "")
        item.target = self
        return item
    }
    
    private func makeQuitItem() -> NSMenuItem {
        let item = NSMenuItem(title: "Quit Still", action: #selector(quit(_:)), keyEquivalent: "")
        item.target = self
        return item
    }
    
    private func makeSeperatorItem() -> NSMenuItem {
        return NSMenuItem.separator()
    }
    
    @objc private func quit(_ sender: NSMenuItem) {
        NSApplication.shared.terminate(self)
    }
    
    func rearrange<T>(array: Array<T>, fromIndex: Int, toIndex: Int) -> Array<T>{
        var arr = array
        let element = arr.remove(at: fromIndex)
        arr.insert(element, at: toIndex)
        return arr
    }
    
}

// menu delegate
extension AppDelegate {
    
    func menuWillOpen(_ menu: NSMenu) {
        reloadMenu()
    }
    
}


// core data
extension AppDelegate {
    
    func save(title: String, completed: Bool) {
        let managedContext = persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Task", in: managedContext)!
        let task = NSManagedObject(entity: entity, insertInto: managedContext)
        task.setValue(title, forKeyPath: "title")
        do {
            try managedContext.save()
            tasks.append(task)
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func fetchTasks() {
        let managedContext = persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Task")
        do {
            tasks = try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    @objc func deleteData() {
        let managedContext = persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Task")
        fetchRequest.returnsObjectsAsFaults = false
        do {
            let results = try managedContext.fetch(fetchRequest)
            for managedObject in results {
                let managedObjectData :NSManagedObject = managedObject as! NSManagedObject
                managedContext.delete(managedObjectData)
            }
            tasks.removeAll()
            reloadMenu()
            statusItem.button?.title = "Replace Me"
            try managedContext.save()
        } catch let error as NSError {
            print("Deleted all my data in myEntity error : \(error) \(error.userInfo)")
        }
    }
}
