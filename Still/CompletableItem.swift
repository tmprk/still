//
//  CompletableItem.swift
//  Still
//
//  Created by Timothy Park on 6/28/19.
//  Copyright © 2019 Timothy Park. All rights reserved.
//

import Cocoa

final class CompletableItem: NSView, NibLoadable {
    
    @IBOutlet weak var titleLabel: NSTextField!
    weak var delegate: AddTaskProtocol?
    var onClick: ((NSClickGestureRecognizer) -> ())?
    
    private var gestureRecognizer: NSClickGestureRecognizer!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        gestureRecognizer = NSClickGestureRecognizer(target: self, action: #selector(clicked(_:)))
        addGestureRecognizer(gestureRecognizer)
    }
    
    override func acceptsFirstMouse(for event: NSEvent?) -> Bool {
        return true
    }
    
    @objc private func clicked(_ sender: NSClickGestureRecognizer) {
        onClick?(sender)
    }
    
    func configure(taskName: String) {
        titleLabel.stringValue = taskName
    }
    
}
