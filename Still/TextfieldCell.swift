//
//  TextfieldCell.swift
//  Still
//
//  Created by Timothy Park on 6/28/19.
//  Copyright © 2019 Timothy Park. All rights reserved.
//

import Cocoa

class TextfieldCell: NSTextFieldCell {
    
    @IBInspectable var borderColor: NSColor = .clear
    @IBInspectable var cornerRadius: CGFloat = 3
    
    override func draw(withFrame cellFrame: NSRect, in controlView: NSView) {
        let bounds = NSBezierPath(roundedRect: cellFrame, xRadius: cornerRadius, yRadius: cornerRadius)
        bounds.addClip()
        super.draw(withFrame: cellFrame, in: controlView)
        if borderColor != .clear {
            bounds.lineWidth = 2
            borderColor.setStroke()
            bounds.stroke()
        }
    }
    
}
