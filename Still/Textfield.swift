//
//  Textfield.swift
//  Still
//
//  Created by Timothy Park on 6/28/19.
//  Copyright © 2019 Timothy Park. All rights reserved.
//

import Foundation
import Cocoa

class Textfield: NSTextField {
    
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
        let bounds: NSRect = self.bounds
        let border: NSBezierPath = NSBezierPath(roundedRect: NSInsetRect(bounds, 0.5, 0.5), xRadius: 3, yRadius: 3)
        NSColor.controlAccentColor.set()
//        NSColor(red: 47 / 255.0, green: 146 / 255.0, blue: 204 / 255.0, alpha: 1.0).set()
        border.stroke()
    }
    
}
